#include <LittleFS.h>
#include "FsValValDb.h"

const char dbName[] = "/strstr.db";

void setup(){

	Serial.begin(115200);
	delay(500);

	Serial.println("Test val->val file DB");

	while( ! LittleFS.begin() ) LittleFS.format();

	ValValDb< String, String, 10> db(LittleFS, dbName );
	//db.remove();

	if ( ! db.init() ) {
		Serial.println(" Can't to create DB");
		return;
	}

	uint index = 0; 
	long ms = millis();

	while ( ! db.add( String(index), String(ms)+"ms") ) {
		Serial.printf("Error add key[%d] value DB\n", index);
		index ++;
		ms = millis();
	}

	Serial.printf("Writed ['%d']'%d' val\n", index, ms);


	String* valP = db.getVal( String(index) );
	if ( nullptr == valP ) {
		Serial.printf("No key[%d] found\n", index);
	} else {
		Serial.printf("val[%d]=%d\n", index, *valP->c_str() );
	}

	long long start = micros();
	for ( int i =0; i<=index; i++){
		db.getVal( String(i) );
	}
	long take = (long) (micros() - start);

	Serial.printf("Read %d records for %d us\n", index+1, take );

	Serial.println( db );
  Serial.printf("%d records in db size %d\n", db.size(), db.fileSize());

}

void loop()
{}