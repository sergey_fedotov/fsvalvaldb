#include <LittleFS.h>
#include "FsValValDb.h"

const char dbName[] = "/telegramUsers.db";

struct TelegramUser {
  long long id = 0;
  long long phone =0;

  bool isEmpty() const {
    return id == 0 && phone == 0; 
  };
};

const TelegramUser telegramUsers[] PROGMEM = {
  TelegramUser{ 301774537LL, 79166014794 },
  TelegramUser{ 999999999LL, 78000000000 },
  TelegramUser{ 978945798LL, 79161535839 },
  {}
};

void setup(){

  Serial.begin(115200);
  delay(500);

  Serial.println("Test telegramUsers file DB");

  while( ! LittleFS.begin() ) LittleFS.format();

  ValValDb<long long, long long, 10> db(LittleFS, dbName );
  //db.remove();

  if ( ! db.init() ) {
    Serial.println(" Can't to create DB");
    return;
  }

  if ( db.size() != 3 ) {
    int i =0;
    TelegramUser user = telegramUsers[i];
    
    while( ! user.isEmpty() ){
      bool res = db.add( user.id, user.phone );
      if ( res) {
        Serial.print("Add user id "); Serial.print( user.id );
        Serial.print(" with phone "); Serial.println( user.phone );
      } else {
        Serial.print("Error add key ");
        Serial.println(user.id );
      }
      user = telegramUsers[++i];;
    }
  }


  Serial.println( db );
  Serial.println( db.size() );

  

  db.remove( 999999999LL );
  Serial.println( db );
  Serial.println( db.size() );

  db.remove( 301774537LL );
  Serial.println( db );
  Serial.println( db.size() );
  
  db.remove( 978945798LL );
  Serial.println( db );
  Serial.println( db.size() );

  Serial.println( db.fileSize() );
  

}

void loop()
{}