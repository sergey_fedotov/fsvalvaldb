#include <LittleFS.h>
#include "FsValValDb.h"

const char dbName[] = "/test.db";

void setup(){

	Serial.begin(115200);
	delay(500);

	Serial.println("Test val->val file DB");

	while( ! LittleFS.begin() ) LittleFS.format();

	ValValDb<uint, long, 50> db(LittleFS, dbName );
	//db.remove();

	if ( ! db.init() ) {
		Serial.println(" Can't to create DB");
		return;
	}

	uint index = 0; 
	long ms = millis();

	while ( ! db.add(index, ms) ) {
		Serial.printf("Error key[%d] already in DB\n", index);
		index ++;
		ms = millis();
	}

	Serial.printf("Writed [%d]'%d' val\n", index, ms);

	long* valP = db.getVal(index);
	if ( nullptr == valP ) {
		Serial.printf("No key[%d] found\n", index);
	} else {
		Serial.printf("val[%d]=%d\n", index, *valP );
	}

	long long start = micros();
	for ( int i =0; i<=index; i++){
		db.getVal( i );
	}
	long take = (long) (micros() - start);

	Serial.printf("Read %d records for %d us\n", index+1, take );

	Serial.println( db );
	Serial.printf(" Db records %d with %d bytes\n", db.size(), db.fileSize() );


	Serial.println("=========== Remove test ================");
	do {
		uint randomKey = micros()%index;
		if ( db.remove( randomKey ) )
			index -= 1;

		Serial.println( db.size());
		Serial.println( db );
	} while( index > 10 );
}

void loop()
{}