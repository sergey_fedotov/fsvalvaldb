#pragma once
#include <Arduino.h>
#include <FS.h>

//#define DEBUG_FS_VAL_VAL
//#define DEBUG_FS_VAL_VAL_put
//#define DEBUG_FS_VAL_VAL_read

#define CacheRecords 20

template<typename KeyT, typename ValT , const uint8_t cacheRecordsSize = CacheRecords >
class ValValDb : public Printable {
public:
	struct KeyValueT {
		KeyT key;
		ValT val;
	};
	struct KeyValuePrint : public Printable {
		const KeyValueT* kv;
		KeyValuePrint( const KeyValueT* kv):
			kv(kv)
			{};
		size_t printTo(Print& p) const {
			size_t out =0;
			if ( nullptr != kv ) {
				out += p.print("[");
				out += p.print(kv->key);
				out += p.print("]");
				out += p.print(kv->val);
				//out += p.print('}');
			}
			return out;
		};	
	};
	size_t printTo(Print& p) const {
		size_t out = 0;
		File db = open("r");
		if ( db ) {
			size_t realReaded;
			do {
				realReaded = db.read( buf, maxCacheBufferSize );
				if ( realReaded < sizeof(KeyValueT) ) break;

				for( size_t pointer = 0; pointer < realReaded ; pointer += sizeof(KeyValueT) ){
					if ( out ) out += p.print(',');
					out += p.print( KeyValuePrint ( (KeyValueT*)&buf[ pointer ] ) );		
				}
			} while( realReaded == maxCacheBufferSize );
			db.close();
		} 
		return out;
	};
	

private:	
	FS& fs;
	const char* dbFileName;
	static constexpr size_t maxCacheBufferSize = cacheRecordsSize*sizeof(KeyValueT);
	uint8_t* buf;
	volatile size_t cachedSize = 0;


	String getDbFolder(){
		String fileName(dbFileName);
		int endFolder = 0;
		endFolder = fileName.lastIndexOf("/");
		return endFolder < 0 ? "/" : fileName.substring(0, endFolder+1);
	};


	void putKeyValueToCache(const KeyValueT& keyVal)  {
#ifdef DEBUG_FS_VAL_VAL
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println(cachedSize);
#endif		
		if ( cachedSize < maxCacheBufferSize ){
			KeyT* key = (KeyT*)&buf[ cachedSize ];
			ValT* val =  (ValT*)&buf[ cachedSize + sizeof(KeyT) ];

			*key = keyVal.key;
			*val = keyVal.val;

			cachedSize += sizeof(KeyValueT);

#ifdef DEBUG_FS_VAL_VAL_put
//		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.printf("Add to cache %d\n", keyVal.key);
#endif			
		}
	};


	KeyValueT* getFromCache( const KeyT& key) const {
		#ifdef DEBUG_FS_VAL_VAL
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println( key );
		#endif		

		KeyValueT* keyVal = nullptr;

		for( size_t pointer = 0; pointer < cachedSize ; pointer += sizeof(KeyValueT) ){

			if ( *(KeyT*)&buf[ pointer ]  == key ) { 

				keyVal = (KeyValueT*)&buf[ pointer ];
				
				#ifdef DEBUG_FS_VAL_VAL_put
				Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
				Serial.printf("Found key %d in cache %d,%d\n", key, keyVal->key, keyVal->val);
				#endif				
				
				break;
			}
		}
		return keyVal;
	};


	size_t delFromCache( KeyValueT* KeyValP){
		uint8_t* startP = (uint8_t*)KeyValP;
		const void* nextP = startP + sizeof(KeyValueT);
		const void* endBuffer = buf + cachedSize;
		const size_t len = buf + cachedSize - (uint8_t*)nextP;
		
		if ( cachedSize < sizeof(KeyValueT) ||
			startP < buf ||
			startP > endBuffer
			)    return cachedSize;

		if ( len > 0 ){
			memmove((void*)startP, nextP, len );
		} 
		return cachedSize - sizeof(KeyValueT);
		
	};

public:	

	ValValDb( FS& fs, const char* fileName):
		fs(fs),dbFileName(fileName)
	{	
#ifdef DEBUG_FS_VAL_VAL
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println( fileName );
#endif			
		buf = (uint8_t*)malloc( maxCacheBufferSize );
	};


	~ValValDb(){
#ifdef DEBUG_FS_VAL_VAL
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println( dbFileName );
#endif		
		if ( nullptr != buf ) delete buf;
	};

	
	bool add(const KeyValueT& keyVal){
#ifdef DEBUG_FS_VAL_VAL
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println( KeyValuePrint(&keyVal) );
#endif
		ValT* val =  getVal(keyVal.key);

		if ( nullptr != val ) {
#ifdef DEBUG_FS_VAL_VAL
			Serial.printf("Error: key='%d' exist already\n", keyVal.key);
#endif
			return false;			
		}

		File db = open("a+");
		
		if ( !db ) {

#ifdef DEBUG_FS_VAL_VAL
			Serial.printf("Error open file='%s'\n", dbFileName);
#endif
			return db;
		}

		size_t writed = db.write( (uint8_t*)&keyVal, sizeof( KeyValueT) );
		db.close();

		if ( writed == sizeof(KeyValueT)) {
			putKeyValueToCache( keyVal );
		}
		return true;
	};
	
	inline bool add(const KeyT& key, const ValT& val){
		return add( KeyValueT{key,val} );
	};

	bool remove(const KeyT& key){
		File db = open("r");
		if ( ! db ) return db;

		String tmpName;
		tmpName += ( millis() & 0xFF );
		tmpName += F("db.tmp");

		File newDb = fs.open(tmpName, "w");
		if ( ! newDb ) {
			db.close();
			return newDb;
		}
#ifdef DEBUG_FS_VAL_VAL
		Serial.printf("Temp file '%s' created\n", tmpName.c_str() );
#endif
		
		do {
#ifdef DEBUG_FS_VAL_VAL_read
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println("\nRead cache from file\n");
#endif			
			cachedSize = db.read( buf, maxCacheBufferSize );
			if ( cachedSize == 0 ) break;

			KeyValueT* keyValP = getFromCache(key);
			size_t writeSize = cachedSize;
			if ( nullptr != keyValP){
				writeSize = delFromCache( keyValP );	
			} 
			size_t writed = newDb.write( buf, writeSize );

		} while( cachedSize == maxCacheBufferSize );

		db.close();
		db.close();
		newDb.close();
		cachedSize =0;
		return fs.rename(tmpName, dbFileName);
	};

	bool removeOnesByOnes(const KeyT& key){
		File db = open("r");
		if ( ! db ) return db;

		String tmpName;
		tmpName += ( millis() & 0xFF );
		tmpName += F("db.tmp");

		File newDb = fs.open(tmpName, "w");
		if ( ! newDb ) {
			db.close();
			return newDb;
		}
#ifdef DEBUG_FS_VAL_VAL
		Serial.printf("Temp file '%s' created\n", tmpName.c_str() );
#endif
		KeyValueT moveRecord;
		size_t readed = 0;
		do {
			readed = db.read( (uint8_t*)&moveRecord, sizeof( KeyValueT) );
			if ( readed && moveRecord.key != key ){
				newDb.write( (uint8_t*)&moveRecord, sizeof( KeyValueT) );
			}
		} while ( readed );

		db.close();
		newDb.close();
		cachedSize =0;
		return fs.rename(tmpName, dbFileName);
	};

	ValT* getVal(const KeyT key){	 
#ifdef DEBUG_FS_VAL_VAL
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println( key );
#endif
		KeyValueT* keyPointer = getKeyVal(key);
		if ( nullptr == keyPointer ) return nullptr;
		else return &keyPointer->val;
	};


	KeyValueT* getKeyVal(const KeyT key){
#ifdef DEBUG_FS_VAL_VAL
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println( key );
#endif		
		KeyValueT* keyVal = getFromCache(key);		
		if ( nullptr != keyVal ) return keyVal;

		File db = open("r");
		if ( ! db ) return keyVal;

		//bool found = false;
		
		do {
#ifdef DEBUG_FS_VAL_VAL_read
		Serial.print( __PRETTY_FUNCTION__ ); Serial.println(__LINE__);
		Serial.println("\nRead cache from file\n");
#endif			
			cachedSize = db.read( buf, maxCacheBufferSize );
			if ( cachedSize == 0 ) break;
			
			keyVal = getFromCache(key);

		} while( nullptr == keyVal && cachedSize == maxCacheBufferSize );

		db.close();
		return keyVal;
	};

	inline File open(const char* type) const {

#ifdef DEBUG_FS_VAL_VAL
		Serial.printf("Open fileName='%s' as '%s'\n", dbFileName, type);
#endif
		return fs.open(dbFileName, type);
	};

	bool init(){
		bool ret = false;
		ret = fs.begin();
		if (ret) {
			String folderName(getDbFolder());

			if ( ! folderName.isEmpty() && ! fs.exists(folderName.c_str()) ){
				ret = fs.mkdir(folderName);				
#ifdef DEBUG_FS_VAL_VAL
				if ( ret ){
					Serial.printf("Folder %s created\n", folderName.c_str());
				} else {
					Serial.printf("Error create folder %s\n", folderName.c_str());
				}
#endif				
			}
		}
		return ret;
	};

	bool remove(){
		bool ret = fs.exists(dbFileName);
		if ( ret ){
			ret = fs.remove(dbFileName);
			cachedSize = 0;
		}
		return ret;
	};

	size_t fileSize(){
		size_t fileSize = 0;
		File db = open("r");
		if ( db ) {
			fileSize = db.size();
			db.close();
		}
		return fileSize;
	};

	inline size_t size(){
		return fileSize()/sizeof( KeyValueT);
	};

};


// template<typename KeyT, typename ValT > //, const uint8_t cacheRecordsSize = CacheRecords>
//size_t ValValDb<KeyT, ValT>::bufSize = CacheRecords*ValValDB::KeyValueT;